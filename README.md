# Nabenik's Enterprise Java basic test

Hi and welcome to this test. As many technical interviews, main test objective is to establish your actual Enterprise coding skills, being:

* Java knowledge
* DDD knowledge
* General toolkits, SDK's and other usages
* Jakarta EE general skills

To complete this test, please create a fork of this repository, fix the source code if required, add your answers to this readme file, commit the solutions/answers to YOUR copy and create a pull request to the original repo.

This document is structured using [GitHub Markdown Flavor](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet#code).

## General questions

1. How to answer these questions?

> Like this

Or maybe with code

```kotlin
fun hello() = "world"
```

2. Please describe briefly the main purpose for the following Jakarta EE specs, also add in your answer http links to actual implementations. Is this project using all specs?

- EJB
- Servlet
- CDI
- JAX-RS

>#####EJB:
> EJB is an architecture for the development and deployment of component-based enterprise applications. Its objective is to provide the programmer with a model that allows him to abstract from the general problems of an enterprise application.
>#####Servlet:
> A Servletes un objeto que recibe una solicitud y genera una respuesta basada en esa solicitud.
> #####CDI:
>Responsible for creating, injecting and destroying the objects of the classes that are going to be used as dependencies and receivers of the same.
> #####JAX-RS:
> Provide support in the creation of web services according to the REST architectural style.

>https://glassfish.org/     https://www.payara.fish/    https://openliberty.io/

>The project uses only JAX-RS and CDI.
> 
3. Which of the following is an application server?

* Open Liberty
* Apache TomEE
* Eclipse Jetty
* Eclipse Glassfish
* Oracle Weblogic

>All except Eclipse Jetty are application servers

4. In your opinion, what's the main benefit of moving from Java 11 to Java 17?
>At the programming language level java 17 has many improvements compared to java 11 (switch expressions, Pattern Matching for instanceof, records, sealed classes) and java 17 has support until 2029.

6. Is it possible to run this project (as is) over Java 17? Why?
>As it is, it is not possible (tested), because there is not 100% compatibility with all dependencies.

8. Is it possible to run this project with GraalVM Native? Why?
>Not compatible.

9. How do you run this project directly from CLI without configuring any application server? Why is it possible?
>I would say it is because you have configured a remote application server.

## Development tasks

To solve this questions please use Gitflow workflow, still, your answers should be in the current branch.

Please also include screenshots on every task. You don't need to execute every task to submit your pull request but feel free to do it :).

0. (easy) Show your terminal demonstrating your installation of OpenJDK 11
>![Image text](images/0.png?raw=true "0")

1. (easy) Run this project using an IDE/Editor of your choice
>![Image text](images/1.1.png?raw=true "1.1")
>![Image text](images/1.2.png?raw=true "1.2")

2. (medium) Execute the movie endpoint operations using curl, if needed please also check/fix the code to be REST compliant
>Create
![Image text](images/2.1.png?raw=true "2.1")

>Update
![Image text](images/2.2.png?raw=true "2.2")

>Read
![Image text](images/2.3.png?raw=true "2.3")

>Delete
![Image text](images/2.4.png?raw=true "2.4")

>List
![Image text](images/2.5.png?raw=true "2.5")


3. (medium) Write an SPA application using Angular, the application should be a basic CRUD that uses Movie operations, upload this application to a new Bitbucket repo and include the link as answer
>https://bitbucket.org/l2ashdz/front-test/src/master/
![Image text](images/3.1.png?raw=true "3.1")
![Image text](images/3.2.png?raw=true "3.2")
![Image text](images/3.3.png?raw=true "3.3")

4. (medium) This project has been created using Java EE APIs, please move it to Jakarta EE APIs and switch it to a compatible implementation (if needed)

5. (hard) Please identify the Integration Test for `MovieRepository`, after that implement each of the non-included CRUD methods
![Image text](images/5.png?raw=true "5")
6. (hard) Please write the Repository and Controller for Actor model, after that implement each of the CRUD methods using an Integration Test
![Image text](images/6.1.png?raw=true "6.1")
![Image text](images/6.2.png?raw=true "6.2")
7. (hard) This project uses vanilla Java EE for Database Persistence, please integrate Testcontainers with MySQL for testing purposes

8. (nightmare) This source code includes only Java EE APIs, hence it's possible to port it to [Open Liberty](https://openliberty.io/). Do it and don't port Integration Tests 

