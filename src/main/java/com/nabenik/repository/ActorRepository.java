package com.nabenik.repository;

import com.nabenik.model.Actor;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.Query;
import java.util.List;

@Stateless
public class ActorRepository {

    @Inject
    EntityManager em;

    public Actor findById(Long id){
        return em.find(Actor.class, id);
    }

    public void create(Actor Actor){
        em.persist(Actor);
    }

    public Actor update(Actor Actor){
        return em.merge(Actor);
    }

    public void delete(Actor Actor){
        em.remove(Actor);
    }

    public List<Actor> listAll(){

        String query = "SELECT a FROM Actor a ";

        Query typedQuery = em.createQuery(query);

        return typedQuery.getResultList();
    }
    
}
