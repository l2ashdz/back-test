package com.nabenik.controller;

import com.nabenik.model.Actor;
import com.nabenik.repository.ActorRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

@Path("/actors")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class ActorController {

    @Inject
    ActorRepository actorRepository;

    @GET
    public Response listAll(){
        List<Actor> actors = actorRepository.listAll();
        return Response.ok().entity(actors).build();
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") Long id){
        Actor actor = actorRepository.findById(id);
        return Response.ok().entity(actor).build();
    }

    @POST
    public Response create(Actor actor){
        actorRepository.create(actor);
        return Response.created(
                        UriBuilder.fromResource(ActorController.class)
                                .path(actor.getActorId().toString()).build())
                .entity(actor).build();
    }

    @PUT
    public Response update(Actor actor){
        Actor ActorU = actorRepository.update(actor);
        return Response.accepted().entity(ActorU).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id){
        Actor actor = actorRepository.findById(id);
        actorRepository.delete(actor);
        return Response.ok().build();
    }
    
}
