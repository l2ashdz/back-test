package com.nabenik.controller;

import com.nabenik.model.Movie;
import com.nabenik.repository.MovieRepository;

import javax.inject.Inject;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.util.List;

@Path("/movies")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class MovieController {


    @Inject
    MovieRepository movieRepository;

    @GET
    public Response listAll(){
        List<Movie> movies = movieRepository.listAll();
        return Response.ok().entity(movies).build();
    }

    @GET
    @Path("/{id}")
    public Response findById(@PathParam("id") Long id){
        Movie movie = movieRepository.findById(id);
        return Response.ok().entity(movie).build();
    }

    @POST
    public Response create(Movie movie){
        movieRepository.create(movie);
        return Response.created(
                UriBuilder.fromResource(MovieController.class)
                .path(movie.getMovieId().toString()).build())
            .entity(movie).build();
    }

    @PUT
    public Response update(Movie movie){
        Movie movieU = movieRepository.update(movie);
        return Response.accepted().entity(movieU).build();
    }

    @DELETE
    @Path("/{id}")
    public Response delete(@PathParam("id") Long id){
        Movie movie = movieRepository.findById(id);
        movieRepository.delete(movie);
        return Response.ok().build();
    }
}
