package com.nabenik.repository;

import com.nabenik.model.Actor;
import com.nabenik.model.Movie;
import com.nabenik.util.EntityManagerProducer;
import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.jboss.shrinkwrap.api.spec.WebArchive;
import org.junit.Test;
import org.junit.runner.RunWith;

import javax.inject.Inject;

import static org.junit.Assert.*;

@RunWith(Arquillian.class)
public class ActorRepositoryTest {

    @Inject
    ActorRepository actorRepository;

    @Deployment
    public static WebArchive createDeployment() {
        return ShrinkWrap.create(WebArchive.class)
                .addClass(Actor.class)
                .addClass(Movie.class)
                .addClass(EntityManagerProducer.class)
                .addClass(ActorRepository.class)
                .addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml")
                .addAsResource("META-INF/persistence.xml");
    }

    @Test
    public void delete() {
        Actor actor = new Actor("actor1", "ciudad", "1997");
        actorRepository.create(actor);
        Long id = actor.getActorId();
        actorRepository.delete(actor);
        Actor result = actorRepository.findById(id);
        assertNull(result);
    }

    @Test
    public void listAll() {
        Actor actor = new Actor("actor1", "ciudad", "1997");
        int size = actorRepository.listAll().size();
        actorRepository.create(actor);
        int sizeAfter = actorRepository.listAll().size();
        assertEquals(sizeAfter, size);
    }

    @Test
    public void findById() {
        Actor actor = new Actor("actor1", "ciudad", "1997");
        actorRepository.create(actor);
        Actor result = actorRepository.findById(actor.actorId);
        assertEquals(actor.getCountry(), result.getCountry());

    }

    @Test
    public void create() {
        Actor actor = new Actor("actor1", "ciudad", "1997");
        actorRepository.create(actor);
        assertNotNull(actor.getActorId());
    }

    @Test
    public void update() {
        Actor actor = new Actor("actor1", "ciudad", "1997");
        actorRepository.create(actor);
        actor.setName("Asael");
        Actor result = actorRepository.update(actor);
        assertEquals("Asael", result.getName());
    }
}
